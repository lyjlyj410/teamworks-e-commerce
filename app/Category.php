<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'title',
        'shortTitle',
        'categoryThumb',
        'categoryHeader'
    ];

    public function classifieds() {
        return $this->hasMany('App\Classified');
    }
    static function categoriesCount() {
        return Category::all()->count();
    }
   
}
