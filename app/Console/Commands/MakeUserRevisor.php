<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class MakeUserRevisor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zteam:MakeUserRevisor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a single user revisor';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $email = $this->ask("Inserisci la mail dell'utente da rendere revisore.");
        $user = User::where('email', $email)->first();

        if (!$user) {
            $this->error("Utente non trovato.");
            return; 
        }

        $user->is_revisor = true;
        $user->save();
        $this->info("L'utente {$user->name} è adesso revisore e guadagnerà automaticamente il 50% dello stipendio di Andrea!");
    }
}
