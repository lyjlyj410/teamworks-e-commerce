<?php

namespace App\Http\Controllers;

use App\Category;
use App\Classified;
use App\ClassifiedImage;
use App\Jobs\ResizeImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\ClassifiedRequest;
use App\Jobs\GoogleVisionLabelImage;
use App\Jobs\GoogleVisionRemoveFaces;
use App\Jobs\GoogleVisionSafeSearchImage;
use App\Jobs\ZTeamWaterMark;
use App\Mail\ClassifiedAdded;
use Illuminate\Support\Facades\Mail;

class ClassifiedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classifieds = Classified::all();
        return view('admin.classifieds.index', compact('classifieds')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();

        $uniqueSecret = base_convert(sha1(uniqid(mt_rand())), 16, 36);
        return view('classifieds.create', compact('categories', 'uniqueSecret'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClassifiedRequest $request)
    {
        $uniqueSecret = $request->input('uniqueSecret');
        $classified = new Classified();

        $classified->title = $request->input('title');
        $classified->description = $request->input('description');
        $classified->category_id = $request->input('category_id');
        $classified->price = $request->input('price');
        $classified->location = $request->input('location');
        $classified->user_id = Auth::user()->id;
        $classified->save();
        
        $images = session()->get("images.{$uniqueSecret}", []);
        $removedImages = session()->get("removedimages.{$uniqueSecret}", []);
 
        if ($images === null){
            return redirect()->back()->with('message', 'Non ci sono immagini Devi aggiungere perlomento un immagine');
        }
       
        $images = array_diff($images, $removedImages);

        foreach ($images as $image) {

            $newImage = new ClassifiedImage();

            $filename = basename($image);
            $newfilename = "public/classifieds/images/{$classified->id}/{$filename}";
            Storage::move($image, $newfilename);

            $newImage->file = $newfilename;
            $newImage->classified_id = $classified->id;
            $newImage->save();

            GoogleVisionSafeSearchImage::withChain([
                new GoogleVisionLabelImage($newImage->id),
                new GoogleVisionRemoveFaces($newImage->id),
                new ZTeamWaterMark($newImage->id),
                new ResizeImage($newImage->file, 400, 300),
            ])->dispatch($newImage->id);

        }
        
        
        File::deleteDirectory(storage_path("/app/public/temp/{$uniqueSecret}"));
        
        // Classified::create([
        //     'title'=>$request->input('title'),
        //     'description'=>$request->input('description'),
        //     'category_id'=>$request->input('category_id'),
        //     'price'=>$request->input('price'),
        //     'location'=>$request->input('location'),
        //     'user_id'=> Auth::id()
        // ]);
        
        Mail::to($classified->user->email)->send(new ClassifiedAdded($classified));
        return redirect()->route('user.profile')->with('message', 'articolo inserito correttamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Classified  $classified
     * @return \Illuminate\Http\Response
     */
    public function show(Classified $classified)
    {
        return view('classifieds.show', compact('classified'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Classified  $classified
     * @return \Illuminate\Http\Response
     */
    public function edit(Classified $classified)
    {
        return view('classifieds.edit', compact('classified'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Classified  $classified
     * @return \Illuminate\Http\Response
     */
    public function update(ClassifiedRequest $request, Classified $classified)
    {
        $classified->update([
            'title'=>$request->input('title'),
            'description'=>$request->input('description'),
            'category_id'=>$request->input('category_id'),
            'price'=>$request->input('price'),
            'location'=>$request->input('location'),
        ]);
        return redirect()->route('classifieds.index')->with('message','Complimenti, annuncio aggiornato con successo!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Classified  $classified
     * @return \Illuminate\Http\Response
     */
    public function delete(Classified $classified)
    {
        $classifiedImages = $classified->classifiedImages;
        foreach ($classifiedImages as $image){
            $image->delete();
        }
        $classified->delete();
        return redirect()->route('classifieds.index')->with('message','Complimenti, annuncio eliminato con successo!');
    }

    public function imageUpload(Request $request)
    {
        $uniqueSecret = $request->input('uniqueSecret');
        
        $filename = $request->file('file')->store("public/temp/{$uniqueSecret}");
        
        dispatch(new ResizeImage(
            $filename,
            120,
            120
        ));
        session()->push("images.{$uniqueSecret}", $filename);
        
        
        return response()->json(
            [
                'id' => $filename
            ]
        );


    }

    public function imageRemove(Request $request)
    {
        $uniqueSecret = $request->input('uniqueSecret');
        $filename = $request->input('id');
        session()->push("removedimages.{$uniqueSecret}", $filename);
        Storage::delete("public/temp/{$uniqueSecret}/$filename");
        return response()->json('ok');
    }

    public function getImages(Request $request){
        $uniqueSecret = $request->input('uniqueSecret');

        $images = session()->get("images.{$uniqueSecret}", []);
        $removedImages = session()->get("removedimages.{$uniqueSecret}", []);

        $images = array_diff($images, $removedImages);
        //$images = $images->diff($removedImages);

        $data = [];

        foreach($images as $image)
        {  
            $data[] = [
                'id' => $image,
                'src' => ClassifiedImage::getUrlByFilePath($image , 120 ,120)
            ] ;


        }
        
        return response()->json($data);
    }

}
