<?php

return [

        'welcome' => 'Welcome to',
        'category' => 'in category',
        'place' => 'place',
        'article-desc' => 'article description',
        'user' => 'user',
        'title' => 'title',
        'description' => 'description',
        'price' => 'price',
        'place' => 'place',
        'images' => 'images',
        'image' => 'image',
        'adult' => 'adult',
        'violence' => 'violence',
        'racy' => 'racy',
        'spoof' => 'spoof',
        'medical' => 'medical',
];