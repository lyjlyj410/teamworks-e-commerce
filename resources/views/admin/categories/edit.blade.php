@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-12">
      <form id="category" action="{{route('categories.update', compact('category'))}}" method="POST" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="form-group">
          <label for="categoryName"> Category Name </label>
          <input value="{{$category->title}}" type="text" name="title" class="form-control" id="categoryName" aria-describedby="emailHelp">
          
        </div>
        <div class="form-group">
          <label for="shortTitle"> Short title </label>
          <input value="{{$category->shortTitle}}" type="text" name="shortTitle" class="form-control" id="shortTitle">
        </div>
        <div class="row">
          <div class="col-12">
            
            
            <div class="row">
              <div class="col-6">
                <div class="w-50 m-auto">
                  
                  <img src="{{ Storage::url($category->categoryThumb) }}" alt="" class="img-fluid">
                </div>
              </div>
              <div class="col-6">
                <div class="custom-file">
                  <label class="custom-file-label" for="categoryThumb">Thumb</label>
                  <input type="file" class="custom-file-input" name="categoryThumb" id="categoryThumb">
                </div>
              </div>
            </div>
            
            
          </div>
        </div>
        <div class="row my-5">
          <div class="col-12">
            
            <div class="row">
              <div class="col-6">
                <div class="w-50 m-auto">

                  <img src="{{ Storage::url($category->categoryHeader) }}" alt="" class="img-fluid">
                </div>
              </div>
              <div class="col-6">
                <div class="custom-file">
                  <label class="custom-file-label" for="categoryHeader">Header</label>
                  <input type="file" class="custom-file-input" name="categoryHeader" id="categoryHeader">
                  
                </div>
              </div>
              
            </div>
          </div>
        </div>
        <div class="row justify-content-center mx-5">
          <div class="col-4">
            <button type="submit" class="btn bg-secondary-custom h2 p-3"><span class="h3 fw-700">Submit</span></button>
          </div>
        </div>
        
        
        
      </form>
    </div>
  </div>
</div>
{{-- {{dd($category->categoryHeader)}} --}}
@endsection