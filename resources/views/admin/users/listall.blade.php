@extends('layouts.app')
@section('content')
<h1 class="text-center mb-3 display-4 fw-700">ADMIN USER LIST </h1>
<div class="container">
    <div class="row">
        <div class="col-12 shadow bg-white mb-5">
            <table class="table table-over mt-3">
                <thead class="thead-dark">
                    <tr class="text-center">
                        <th scope="col">
                            ID 
                        </th>
                        <th scope="col">
                            <span class="">Avatar</span>
                        </th>
                        <th scope="col">
                            Nome
                        </th>
                        <th scope="col">
                            Email
                        </th>
                        <th scope="col">
                            REVISOR
                        </th>
                        <th scope="col">
                            ADMIN
                        </th>
                        <th scope="col">
                           Rev REQ
                        </th>
                        <th scope="col">
                            make Revisor
                        </th>
                        <th scope="col">
                            Appr
                        </th>
                        <th scope="col">
                            Refus
                        </th>
                        <th scope="col">
                           Pending
                        </th>
                        <th scope="col">
                           Profile
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    @php
                    
                    
                    @endphp
                    <tr>
                        <td class="text-center h3" style="vertical-align: middle">
                            {{$user->id}}
                        </td>
                        <td>  <img src="{{Storage::url($user->avatar)}}" class="img-fluid" alt=""></td>
                        <td class="lead">{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        
                            @if ($user->is_revisor === 1)
                            <td><div class="bg-success m-1 p-3"></div></td>
                            @else 
                            <td></td>
                            @endif
                      
                        
                            @if ($user->is_admin === 1)
                            <td>
                                <img src="/images/logo1.png" class="img-fluid bg-main" alt="">
                            </td>
                            @else 
                            <td></td>
                            @endif
                            
                        
                        
                        @php
                        $check = \App\RevisorRequest::where('user_email', $user->email)->get();
                        
                        
                        @endphp
                        
                        
                        
                        @if (count($check) > 0)
                        <td><div class="bg-success m-1 p-3"></div></td>
                        @else 
                        <td></td>
                        @endif
                        
                        <td>
                            <a class="btn btn-primary" href="{{route('admin.makeUserRevisor', $user->id
                            )}}">Rendi Revisore</a>
                        </td>
                        <td class="text-center h3" style="vertical-align: middle">
                            {{\App\Classified::userAppovedAdsCount($user)}}
                        </td>
                        <td class="text-center h3" style="vertical-align: middle">
                            {{\App\Classified::userRefusedAdsCount($user)}}
                        </td>
                        <td class="text-center h3" style="vertical-align: middle">
                            {{\App\Classified::userPendingAdsCount($user)}}
                        </td>
                        <td>
                            
                            <a class="btn btn-primary" href="{{route('admin.user.profile', $user->id)}}">Vai Al Profilo</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
