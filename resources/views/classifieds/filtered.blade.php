@extends('layouts.app')
@section('content')
<div class="container">
    <img src="{{ Storage::url($category->categoryHeader) }}" alt="" class="img-fluid">
</div>
<div class="container pb-5 pt-1">
    <div class="row">
        
        <div class="col-12">
            <h2>Totale nella categoria: {{$category->title}} {{App\Classified::categoryAdsCount($category)}}</h2>
        </div>
        
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-12">
            @foreach($classifieds as $classified)
            <div class="card mb-3 border-0 shadow">
                <div class="row no-gutters">
                    <div class="col-md-3 p-3">
                        @php
                        $image = $classified->classifiedImages->pop();  
                        @endphp 
                        <a href="{{route('classifieds.show', compact('classified'))}}">
                            @if ($image === null)
                            
                            @else
                            <img src="{{$image->geturl(400,300)}}" class="card-img img-fluid" alt="{{ $classified->title }}">

                            @endif
                        </a>
                    </div>
                    <div class="col-md-9">
                        <div class="card-body">
                            <h5 class="card-title mt-3">{{ $classified->title }}</h5>
                            <p class="card-text">Descrizione {{ $classified->description }}</p>
                            <p class="card-text">Località: {{ $classified->location }}</p>
                            <p class="card-text">Prezzo: {{ $classified->price }}</p>
                            <p class="card-text"> Nella Categoria: <a href="{{route('classifieds.categoryFiltered', $classified->category_id)}}">{{ $classified->category->title}}</a></p>
                            <p class="card-text"> Venduto da: <a href="{{route('user.article', $classified->user->id)}}">{{ $classified->user->name}}</a></p>
                            <a href="{{route('classifieds.show', compact('classified'))}}" class="btn btn-dark">Vai all'annuncio</a>
                            @if (Auth::user() && Auth::user()->is_admin)
                            <a href="{{route('single.classified.images', compact('classified'))}}" class="btn btn-danger ml-5">Edita Immagini</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div> 

@endsection