@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h3 class="text-center my-3">Lista immagini articolo {{$classified->name}} ID {{$classified->id}} 
        
            </h3>
            @if(Auth::user()->id == $classified->user_id)
            <a href="{{route('classified.new.image', compact('classified'))}}" class="btn btn-primary mb-3">Nuova immagine</a>
            @endif
        </div>
    </div>
</div>
@php 
    
@endphp
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="row">
                @foreach ($images as $image)
                    <div class="col-3">
                        <img src="{{Storage::url($image->file)}}" class="img-fluid" alt="">
                        <span class="lead">ID {{$image->id}}</p>
                        @if(Auth::user() && Auth::user()->id == $image->classified->user->id)
                        <button class="btn btn-success">Edita Immagine</button>
                        <form action="{{route('classified.images.delete', compact('image'))}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Elimina immagine</button>
                                
                            </form>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection