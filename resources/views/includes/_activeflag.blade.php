<div class="col-4">
    @include('includes._flag', ['lang' => 'it', 'nation' => 'it'])
</div>                    

<div class="col-4">
    
    
    @include('includes._flag', ['lang' => 'en', 'nation' => 'gb'])
    
</div>

<div class="col-4">
    
    @include('includes._flag', ['lang' => 'es', 'nation' => 'es'])
    
</div>