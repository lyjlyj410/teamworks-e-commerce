


<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    
    <!-- Bottone home -->
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img id="logo" src="/images/logo.png" alt="Z-Market" class="img-fluid">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        
        
        
        <!-- Form di search -->
        <div class="mx-auto w-50">
            
            <div class="row">
                <div class="col-12">
                    <form action="{{ route('search') }}" method="GET" class="nav-searchBar">
                        @csrf 
                        <div class="row">
                            
                            <div class="col-6 px-2">
                                <input type="text" placeholder="Cerca tra gli annunci" name="q" id="searchForm" class="shadow ">
                            </div>
                            
                            <div class="col-4 box">
                                <select name="category_id" id="category_id" value="{{ old('category_id') }}"     class=" @error('category_id') is-invalid @enderror"            aria-describedby="category_id">
                                    <option value="" selected>Tutte Le Categorie</option>
                                    @foreach($categories as $category)
                                    <option value="{{ $category->id }}"><h3>{{ $category->title }}</h3></option>
                                    @endforeach
                                </select> 
                            </div>
                            
                            <div class="col-2 justify-content-right">
                                <button class="btn rounded-1 shadow m-0" id="searchButton" type="submit"> <i class="fas fa-search iconSearch"></i> </button>
                            </div>
                          
                        </div>
                    </form>
                        
                        
                        
                    </div>
                    
                </div>
            </div>
            
            
            <ul class="navbar-nav w-25">
             
                <li class="nav-item px-3">
                    <div class="d-flex h-100">
                        <a class="btn secondaryColor bg-main align-self-center" href="{{ route('classifieds.create') }}"> Crea annuncio</a>
                    </div>
                    
                </li>
                @guest
                <li class="nav-item">
                    <div class="d-flex h-100">  
                        <a class="nav-link align-self-center" href="{{ route('login') }}">{{ __('auth.login') }}</a>
                    </div>
                </li>
                @if (Route::has('register'))
                <li class="nav-item">
                    <div class="d-flex h-100">  

                        <a class="nav-link align-self-center" href="{{ route('register') }}">{{ __('auth.register') }}</a>
                    </div>
                </li>
                @endif
                @else
                
                @include('includes._loggeduserbutton')
        @endguest
        <li class="nav-item">
            <div class="row flex-column">
                
                @include('includes._activeflag')
                
            </div>
            
            
            
        </li>
    </ul>
    
</div>


</div>

   


</nav>
@php 
    $url = Request::url();
    
@endphp
{{-- stampa solo se non siamo sulla home --}}
@if ( $url == 'http://127.0.0.1:8000')
 
 @else 
 <div class="container pt-3">
    <div class="row">
        <div class="col-12">
            <h1 class="h3 text-center">{{__('ui.welcome')}} Z-Market
                @if (Auth::user())
                {{Auth::user()->name}}
                @endif
            <span class="h4">L'ultimo sito di annunci di cui avrai mai bisogno!</span>   </h1>  
        </div>
    </div>
</div>
@endif



