@extends('layouts.app')
@section('content')

@php
if(Auth::user()) {
    $user = Auth::user();
}
@endphp
<header class="masthead">
  <div class="container h-100">
    <div class="row h-100 align-items-center">
      <div class="col-8 white">
        <h1 class="display-4">Vuoi guadagnare lavorando da casa?</h1>
        <h2>Diventa revisore di Z-Market!</h2>
        <p class="h4 py-2">Bastano pochi minuti al giorno per crearsi un introito mensile interessante! Vuoi entrare a far parte del nostro team?</p>
        <a href="#" class="btn btn-warning my-3 p-2">Scopri di più</a>
      </div>
      <div class="col-4"></div>
    </div>
  </div>
</header>


<div class="container py-5">

    <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class="lpRevisorBox">
                <div>
                    <i class="fa fa-globe fa-3x mb-3"></i>
                </div>
                <h3>Dove vuoi</h3>
                        <p>
                           Lavori da dove vuoi! Da casa, dal lavoro, dalla spiaggia od ovunque tu voglia!
                        </p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="lpRevisorBox">
                        <div>
                            <i class="far fa-clock fa-3x mb-3"></i>
                        </div>
                        <h3>Quanto vuoi</h3>
                        <p>
                            Non c'è un minimo tempo richiesto. Guadagni in base agli annunci approvati o rifiutati!
                        </p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="lpRevisorBox">
                        <div>
                            <i class="fa fa-rocket fa-3x mb-3"></i>
                        </div>
                        <h3>Quando vuoi</h3>
                        <p>
                            Hai voglia di iniziare oggi? Basta fare la richiesta e la tua candidatura sarà analizzata entro 24 ore!
                        </p>
                    </div>
                </div>
            </div>

</div>

<div class="container-fluid py-5 bg-light">
    <div class="row">
        <div class="col-12">


            <div class="container py-4">
                <div class="row">
                    <div class="col-12 text-center"> 
                        <p class="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati, laboriosam sapiente porro debitis non cumque ipsum dignissimos temporibus quis nesciunt in ipsa mollitia, vero, molestias ab repellat quos tenetur! Tempore.</p>
                        @if(Auth::user())
                        <a href="{{route('revisor.request', compact('user'))}}" class="btn btn-lg btn-success">Diventa revisore</a>
                        @else
                        <a href="{{route('register')}}" class="btn btn-lg btn-success">Registrati e diventa revisore</a>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection