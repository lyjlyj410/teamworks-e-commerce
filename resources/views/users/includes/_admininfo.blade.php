<div class="d-block my-3">
    @if (Auth::user() && Auth::user()->is_admin)
    <h4>
        <span>Tot </span><i class="fas fa-users"></i>
        <span class="badge badge-pill badge-danger">
            {{ \App\User::userCount() }}
        </span>
    </h4>
    <h4>
        <span>Tot </span><i class="fas fa-images"></i>
        <span class="badge badge-pill badge-danger">
            {{ \App\Classified::totalUserImageLoad($user) }}
        </span>
    </h4>
    <h4>
        <span>Richieste per Revisore</span> 
        <span class="badge badge-pill badge-danger">
            {{ \App\revisorRequest::toBeCheckCount() }}
        </span>
    </h4>
    <h4>
        <i class="fas fa-user-secret"></i>
        <span class="badge badge-pill badge-success">
            {{ \App\revisorRequest::totalRevisor() }}
        </span>
    </h4>
    @endif
</div>
<div class="d-block my-3">
    @if (Auth::user() && Auth::user()->is_admin)
    <h4>

        <span>Totale Articoli</span> <i class="fas fa-ad"></i>
        <span class="badge badge-pill badge-danger">
            {{ \App\Classified::totalCount() }}
        </span>
    </h4>
    <h4>

        <span>Da revisionare</span> <i class="fas fa-ad text-danger"></i>
        <span class="badge badge-pill badge-success">
            {{ \App\Classified::toBeRevisedCount() }}
        </span>
    </h4>
    @endif
</div>