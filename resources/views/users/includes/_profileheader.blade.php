<div class="container py-5">
    <div class="row">
        <div class="col-12 bg-white shadow">
            
            <div class="row py-3">
                <div class="col-10 d-flex align-items-center" id="userprofilebar">
                    @if (Auth::user() && Auth::user()->is_admin)
                    <span class="h1 h-100 d-flex bg-main">
                        
                        <span class="badge align-self-center text-secondary-zteam ">
                            {{__('ui.welcome')}} tuo <br> Profilo
                        </span>
                    </span>
                    <a href="" class="text-dark">
                        <span class="h2 px-3">
                            <span></span><i class="fas fa-users"></i>
                            <span class="h2">
                                <span class="badge  px-3 py-1 badge-danger">
                                    {{ \App\User::userCount() }}
                                </span>
                            </span>
                        </span>
                    </a>
                    <a class="text-danger" href="{{ route('admin.revisor.userRequestList') }}">
                        <span class="h2 px-3">
                            <i class="fas fa-user-secret"></i>
                            <span class="h2">
                                <span class="badge  px-3 py-1 badge-danger">
                                    {{ \App\revisorRequest::toBeCheckCount() }}
                                </span>
                            </span>
                        </span>
                    </a>
                    <a class="text-danger" href="{{ route('revisor.home') }}">
                        <span class="h2 px-3">
                            <i class="fas fa-ad"></i>
                            <span class="h2">
                                <span class="badge  px-3 py-1 badge-danger">
                                    {{ \App\Classified::toBeRevisedCount() }}
                                </span>
                            </span>
                        </span>
                    </a>
                   <a href="" class="text-dark">
                    <span class="h2 px-3">
                        <span></span><i class="fas fa-images"></i>
                        <span class="badge px-3 py-1 badge-danger">
                            {{ \App\Classified::totalUserImageLoad($user) }}
                        </span>
                    </span>
                   </a>
                   <a href="" class="text-dark">
                    <span class="h2 px-3">
                        <span>All</span><i class="fas fa-images"></i>
                        <span class="badge px-3 py-1 badge-danger">
                            {{ \App\ClassifiedImage::totalCount() }}
                        </span>
                    </span>
                   </a>
                </div>
                <div class="col-2">
                    <a class="text-success" href="{{ route('admin.users.list') }}">
                        <h4>
                            
                            <i class="fas fa-ad"></i>
                            <span class="badge px-3 py-1 badge-info">
                                {{ \App\Classified::totalCount() }}
                            </span>
                        </h4>
                    </a>
                    <h4>
                        <i class="fas fa-user-secret text-success"></i>
                        <span class="badge px-3 py-1 badge-success">
                            {{ \App\revisorRequest::totalRevisor() }}
                        </span>
                        
                    </h4>
                    
                </div></div>
                @elseif (Auth::user() && Auth::user()->is_revisor)
                <span class="h1">
                    <span class="badge badge-success">
                        Benvenuto Revisore
                    </span>
                </span>
                <span class="h2 px-3">
                    <i class="fas fa-ad text-danger"></i>
                    <span class="h2">
                        <span class="badge  px-3 py-1 badge-danger">
                            {{ \App\Classified::toBeRevisedCount() }}
                        </span>
                    </span>
                </span>
                <span class="h2 px-3">
                    <span>Tot </span><i class="fas fa-images"></i>
                    <span class="badge px-3 py-1 badge-danger">
                        {{ \App\Classified::totalUserImageLoad($user) }}
                    </span>
                </span></div></div>
                @elseif (Auth::user())
                <span class="h1">
                    <span class="badge badge-success">
                        Benvenuto User
                    </span>
                </span>
                <span class="h2 px-3">
                    <span>Tot </span><i class="fas fa-images"></i>
                    <span class="badge px-3 py-1 badge-danger">
                        {{ \App\Classified::totalUserImageLoad($user) }}
                    </span>
                </span>
                <span class="h2 px-3">
                    <a class="btn secondaryColor bg-main align-self-center" href="{{ route('classifieds.create') }}">
                        <span class="h3">Crea Annuncio </span><i class="fas fa-ad fa-2x"></i>
                        
                    </a>
                </span>
            </div></div>
            @endif
            
        </div>
        
        
    </div>
</div>
</div>